// About me:
// 1990.
// No degree (cources) in 2012 (also some webdevelopment/webmastering before). Also: bakery, theaters, journalism, and other things.
// My tags: opensource (GNU/Linux, Vim), healthy lifestyle (bycicle, nutrition, exercises), self-development
//
//
//

package main

import (
	"fmt"
	"net/http"
)

// https://golang.org/doc/articles/wiki/
// https://golang.org/pkg/net/http/
func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		var token string = r.URL.Query().Get("token")
		if len(token) > 0 {
			fmt.Fprintf(w, "Your token is %v\n", token)
		} else {
			fmt.Fprint(w, "Hello World.")
		}
	})

	http.HandleFunc("/users/vitaly/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprint(w, "Hello Vitaly.")
	})

	http.Handle("/files/", http.StripPrefix("/files/", http.FileServer(http.Dir("/Users/vitaly/for-it-academy-by/"))))

	http.ListenAndServe(":80", nil)
}
